
package com.fie.cognos.cursojava.soap.client;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "HolaMundoService", targetNamespace = "http://services.soap.cursojava.cognos.fie.com/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface HolaMundoService {


    /**
     * 
     * @param materno
     * @param paterno
     * @param nombre
     * @param edad
     * @return
     *     returns com.fie.cognos.cursojava.soap.client.Persona
     */
    @WebMethod
    @WebResult(name = "persona", partName = "persona")
    @Action(input = "http://services.soap.cursojava.cognos.fie.com/HolaMundoService/cargarPersonaRequest", output = "http://services.soap.cursojava.cognos.fie.com/HolaMundoService/cargarPersonaResponse")
    public Persona cargarPersona(
        @WebParam(name = "nombre", partName = "nombre")
        String nombre,
        @WebParam(name = "paterno", partName = "paterno")
        String paterno,
        @WebParam(name = "materno", partName = "materno")
        String materno,
        @WebParam(name = "edad", partName = "edad")
        int edad);

    /**
     * 
     * @param arg0
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://services.soap.cursojava.cognos.fie.com/HolaMundoService/saludarRequest", output = "http://services.soap.cursojava.cognos.fie.com/HolaMundoService/saludarResponse")
    public String saludar(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0);

}
