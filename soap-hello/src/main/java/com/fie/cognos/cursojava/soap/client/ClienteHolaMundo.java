package com.fie.cognos.cursojava.soap.client;


public class ClienteHolaMundo {

	public static void main(String[] args) {
		
		
		HolaMundoServiceService service = new HolaMundoServiceService();
		HolaMundoService cliente = service.getHolaMundoServicePort();
		System.out.println(cliente.saludar("Ariel"));
		
		Persona persona =  cliente.cargarPersona("Ariel", "Escobar", "Endara", 12);
		System.out.println("DATOS PERSONA");
		System.out.println("NOMBRE: " + persona.getNombre());
		System.out.println("PATERNO: " + persona.getPaterno());
		System.out.println("MATERNO: " + persona.getMaterno());
		System.out.println("EDAD: " + persona.getEdad());
	}

}
