package com.fie.cognos.cursojava.soap.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.xml.ws.WebServiceContext;

import com.fie.cognos.cursojava.soap.entities.Persona;

@WebService
@SOAPBinding(style = Style.RPC, use = Use.LITERAL)
public class HolaMundoService {

	
	
	@WebMethod
	public String saludar(String nombre) {
		return "Hola " + nombre;
	}
	
	@WebMethod(operationName = "cargarPersona")
	@WebResult(name = "persona")
	public Persona cargarPersona(@WebParam(name = "nombre")String nombre, @WebParam(name = "paterno")String paterno, 
			@WebParam(name = "materno")String materno, @WebParam(name = "edad")int edad) {
		return new Persona(nombre, paterno, materno, edad);
	}
}
